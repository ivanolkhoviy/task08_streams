package com.olkhoviy.task04;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

public class Task4 {

    private static Logger log = LogManager.getLogger(Task4.class);


    public long countUniqueWords(String text) {
        log.info("Count number of unique words");
        return Arrays.stream(text.split("[ \n]"))
                .map(String::trim)
                .distinct()
                .count();

    }

    public List<String> uniqueWordsSorted(String text) {
        log.info("Sorted list of all unique words");
        return Arrays.stream(text.split("[ \n]"))
                .map(String::trim)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    public Map<String, Integer> countOccurrenceOfWords (String text) {
        log.info(" Occurrence number of each word in the text");
        return Arrays.stream(text.split("[ \n]"))
                .map(word -> new AbstractMap.SimpleEntry<>(word, 1))
                .collect(toMap(e -> e.getKey(), e -> e.getValue(), (v1, v2) -> v1 + v2));
    }

    public Map<String, Long> countOccurrenceOfSymbols(String text) {
        return Arrays.stream(text.split(""))
                .filter(character -> character.matches("[a-z]"))
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()));
    }

}
