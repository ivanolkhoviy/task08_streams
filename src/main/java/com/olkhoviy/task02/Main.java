package com.olkhoviy.task02;

import com.olkhoviy.task02.commands.LockCommand;
import com.olkhoviy.task02.model.Device;
import com.olkhoviy.task02.model.Laptop;
import com.olkhoviy.task02.model.PersonalComputer;

import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Main {
    private static Logger log = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        User user = new User();
        Device pc = new PersonalComputer();
        Device laptop = new Laptop();
        log.info("Execute operation as object of command class");
        user.executeOperation(new LockCommand(new Laptop()));
        log.info("Execute operation with lambda expression");
        user.executeOperation(()->pc.turnOn());

        log.info("Execute operation with method reference");
        user.executeOperation(laptop::restart);

        log.info("Execute operation from anonymous class");
        Device laptop1 = new Laptop(){
            @Override
            public String turnOff() {
                System.out.println("turn off from abstract class");
                return "ss";
            }
        };
        laptop1.turnOff();


    }
}