package com.olkhoviy.task02.commands;

import com.olkhoviy.task02.model.Device;

public class TurnOffCommand implements Command {

    private Device device;

    public TurnOffCommand(Device device) {
        this.device = device;
    }

    @Override
    public void execute() {
        device.turnOff();
    }


}
