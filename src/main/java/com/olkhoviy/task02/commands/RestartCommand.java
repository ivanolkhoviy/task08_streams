package com.olkhoviy.task02.commands;

import com.olkhoviy.task02.model.Device;

public class RestartCommand implements Command {

    private Device device;

    public RestartCommand(Device device) {
        this.device = device;
    }

    @Override
    public void execute() {
        device.restart();
    }

    @Override
    public String toString() {
        return "RestartCommand{" +
                "device=" + device +
                '}';
    }
}