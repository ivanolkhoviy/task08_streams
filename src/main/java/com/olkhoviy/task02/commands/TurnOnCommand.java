package com.olkhoviy.task02.commands;

import com.olkhoviy.task02.model.Device;

public class TurnOnCommand implements Command {

    private Device device;

    public TurnOnCommand(Device device) {
        this.device = device;
    }

    @Override
    public void execute() {
        device.turnOn();
    }
}