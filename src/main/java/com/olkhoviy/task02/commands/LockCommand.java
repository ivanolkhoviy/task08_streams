package com.olkhoviy.task02.commands;

import com.olkhoviy.task02.model.Device;
import com.olkhoviy.task02.model.Laptop;

public class LockCommand implements Command {

        Device device;

        public LockCommand(Laptop device) {
            this.device = device;
        }

        @Override
        public void execute() {
            device.lock();
        }

        @Override
        public String toString() {
            return "LockCommand{" +
                    "device=" + device +
                    '}';
        }
    }
