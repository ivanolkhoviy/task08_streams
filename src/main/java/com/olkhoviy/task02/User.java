package com.olkhoviy.task02;

import com.olkhoviy.task02.commands.Command;
import com.olkhoviy.task02.model.Device;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class User {
    private static Logger log = LogManager.getLogger(User.class);
    public final List<Command> operations = new ArrayList<>();



    public void executeOperation(Command command) {
        operations.add(command);
        log.info("Execute operation: " + command.toString());
    }

    public void lock(Device device){
        device.lock();
    }

    public void turnOff(Device device){
        device.turnOff();
    }
}