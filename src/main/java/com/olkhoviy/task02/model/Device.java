package com.olkhoviy.task02.model;

public abstract class Device {


    public abstract void turnOn();

    public abstract String turnOff();

    public abstract void restart();

    public abstract void lock();


}