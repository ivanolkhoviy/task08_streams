package com.olkhoviy.task02.model;

import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Laptop extends Device {

    private static Logger log = LogManager.getLogger(Laptop.class);


    @Override
    public void turnOn() {
        log.info("Laptop is turnOn");
    }

    @Override
    public String turnOff() {
        log.info("Laptop is turnOff");
        return "Laptop turnOff";
    }

    @Override
    public void restart() {
        log.info("Laptop restarted");
    }

    @Override
    public void lock() {
        log.info("Laptop is locked");
    }

    @Override
    public String toString() {
        return "Laptop{}";
    }
}
