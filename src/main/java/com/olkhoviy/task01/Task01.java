package com.olkhoviy.task01;


import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Task01 {
    private static Logger log = LogManager.getLogger(Task01.class);

    public static void main(String[] args) {

        App max = (a, b, c) -> Integer.max(Integer.max(a, b), c);
        int x = max.compute(45, 12, 27);
        log.info("max= " + x);

        App average = (a, b, c) -> (a + b + c) / 3;
        int a = average.compute(8, 16, 32);
        log.info("average= " + a);
    }
}
