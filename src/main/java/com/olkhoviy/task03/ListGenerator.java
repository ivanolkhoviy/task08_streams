package com.olkhoviy.task03;

import java.util.Arrays;
import java.util.List;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ListGenerator {
    private static Logger log = LogManager.getLogger(ListGenerator.class);


    public List<Integer> makeStreamFromArray() {
        log.info("Generate stream using Arrays.asList");
        Stream<Integer> stream = Arrays.asList(5, 7, 54, 12, 23, 44, 61).stream();
        return stream.collect(Collectors.toList());
    }

    public List<Integer> makeStreamOf() {
        log.info("Generate stream using Stream.of");
        Stream<Integer> stream = Stream.of(32, 11, 19, 42, 67, 83, 25);
        return stream.collect(Collectors.toList());
    }

    public List<Integer> useStreamBuilder() {
        log.info("Generate stream using StreamBuilder");
        Stream<Integer> stream = Stream
                .<Integer>builder()
                .add(23)
                .add(48)
                .add(35)
                .add(72)
                .add(16)
                .build();
        return stream.collect(Collectors.toList());
    }

    public List<Integer> useStreamIterator() {
        log.info("Generate stream using Stream.iterate");
        Stream<Integer> stream = Stream
                .iterate(3, n -> n * 2)
                .limit(8);
        return stream.collect(Collectors.toList());
    }

}